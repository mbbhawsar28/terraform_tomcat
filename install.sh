#!/bin/bash
sudo yum update -y
sudo yum install -y java
sudo su
cd /opt
wget https://dlcdn.apache.org/tomcat/tomcat-8/v8.5.98/bin/apache-tomcat-8.5.98.tar.gz
tar -xvzf /opt/apache-tomcat-8.5.98.tar.gz
chmod +x /opt/apache-tomcat-8.5.98/bin/startup.sh
chmod +x /opt/apache-tomcat-8.5.98/bin/shutdown.sh
ln -s /opt/apache-tomcat-8.5.98/bin/startup.sh /usr/local/bin/tomcatup
ln -s /opt/apache-tomcat-8.5.98/bin/shutdown.sh /usr/local/bin/tomcatdown
tomcatup

cd /opt/apache-tomcat-8.5.98/webapps
wget https://tomcat.apache.org/tomcat-7.0-doc/appdev/sample/sample.war

sudo bash -c 'cat <<EOL > /etc/tomcat8/tomcat-users.xml
<?xml version="1.0" encoding="UTF-8"?>
<tomcat-users xmlns="http://tomcat.apache.org/xml"
              xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
              xsi:schemaLocation="http://tomcat.apache.org/xml tomcat-users.xsd"
              version="1.0">
<role rolename="manager-gui"/>
<role rolename="manager-script"/>
<role rolename="manager-jmx"/>
<role rolename="manager-status"/>
<user username="admin" password="admin" roles="manager-gui, manager-script, manager-jmx, manager-status"/>
<user username="deployer" password="deployer" roles="manager-script"/>
<user username="tomcat" password="s3cret" roles="manager-gui"/>
</tomcat-users>
EOL'
TOMCAT_HOME="/opt/apache-tomcat-8.5.98"

# Edit context.xml for /opt/apache-tomcat-8.5.98/webapps/docs
DOCS_CONTEXT_FILE="$TOMCAT_HOME/webapps/docs/META-INF/context.xml"
sudo sed -i 's|<Context antiResourceLocking="false" >|<Context antiResourceLocking="false" >\n        <!--  <Valve className="org.apache.catalina.valves.RemoteAddrValve" allow="127\\.\d+\\.\d+\\.\d+|::1|0:0:0:0:0:0:0:1" /> -->|' $DOCS_CONTEXT_FILE

# Edit context.xml for /opt/apache-tomcat-8.5.98/webapps/host-manager
HOST_MANAGER_CONTEXT_FILE="$TOMCAT_HOME/webapps/host-manager/META-INF/context.xml"
sudo sed -i 's|<Context antiResourceLocking="false" privileged="true" >|<Context antiResourceLocking="false" privileged="true" >\n  <CookieProcessor className="org.apache.tomcat.util.http.Rfc6265CookieProcessor" sameSiteCookies="strict" />\n  <Valve className="org.apache.catalina.valves.RemoteAddrValve" allow="127\\.\d+\\.\d+\\.\d+|::1|0:0:0:0:0:0:0:1" />\n  <Manager sessionAttributeValueClassNameFilter="java\\.lang\\.(?:Boolean|Integer|Long|Number|String)|org\\.apache\\.catalina\\.filters\\.CsrfPreventionFilter\\$LruCache(?:\\$1)?|java\\.util\\.(?:Linked)?HashMap"/>|' $HOST_MANAGER_CONTEXT_FILE

# Edit context.xml for /opt/apache-tomcat-8.5.98/webapps/manager
MANAGER_CONTEXT_FILE="$TOMCAT_HOME/webapps/manager/META-INF/context.xml"
sudo sed -i 's|<Context antiResourceLocking="false" privileged="true" >|<Context antiResourceLocking="false" privileged="true" >\n  <CookieProcessor className="org.apache.tomcat.util.http.Rfc6265CookieProcessor" sameSiteCookies="strict" />\n  <!--  <Valve className="org.apache.catalina.valves.RemoteAddrValve" allow="127\\.\d+\\.\d+\\.\d+|::1|0:0:0:0:0:0:0:1" />-->\n  <Manager sessionAttributeValueClassNameFilter="java\\.lang\\.(?:Boolean|Integer|Long|Number|String)|org\\.apache\\.catalina\\.filters\\.CsrfPreventionFilter\\$LruCache(?:\\$1)?|java\\.util\\.(?:Linked)?HashMap"/>|' $MANAGER_CONTEXT_FILE
