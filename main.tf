provider "aws" {
  version = "~> 5.0"
  
    region = "us-east-1"
    access_key = "${var.access_key}"
    secret_key = "${var.secret_key}"
}
resource "aws_instance" "app_server" {
  ami           = "ami-0005e0cfe09cc9050" 
  instance_type = "t2.micro"

  tags = {
    Name = "tomcat-server"
  }
 user_data = <<-EOF
              #!/bin/bash
              sudo yum update -y
              sudo yum install java -y
              sudo yum install -y git
              cd /opt
              git clone "https://gitlab.com/mbbhawsar28/terraform_tomcat.git"
              cd /opt/terraform_tomcat/install.sh 
              sh install.sh
              EOF


}

resource "aws_security_group" "allow_tomcat" {
  name        = "allow_tomcat"
  description = "Allow inbound traffic to Tomcat"

  ingress {
    from_port = 8080
    to_port   = 8080
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
